 import { Component, OnInit,ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { AddProjectComponent } from './add-project/add-project.component';
import { ProjectService } from './service/project.service';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import { CoreService } from './core/core.service';
import { Project } from './projects';
import { ConfirmationDialogComponent } from './confirmation-dialog/confirmation-dialog.component';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent  implements OnInit{
  title = 'OrangeAngular';
  projects: Project[] = [];
  displayedColumns: string[] = [
    'id', 
    'title',
     'completed_at', 
     'created_at',
     'updated_at',
     'deleted_at',
     'action'];
  dataSource!: MatTableDataSource<Project>;

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;

  constructor(private _dialog:MatDialog, 
    private _projectservce:ProjectService,
    private _coreservice:CoreService,
    
  ){}
  addProject(){
   const dialogRef= this._dialog.open(AddProjectComponent);
   dialogRef.afterClosed().subscribe({
    next:(val)=>{
      if(val){
        this.getProjects();
      }
    },
   });
  }

  ngOnInit():void
  {
     this.getProjects();
    
 }
  getProjects(){
    this._projectservce.getProjects().subscribe({
      next:(projects: Project[])=>{
        this.dataSource=new MatTableDataSource(projects);
        this.dataSource.sort=this.sort;
        this.dataSource.paginator=this.paginator;
      },
      error:console.log,
      
    })
  }
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
  // deleteProject(id:number){
  //   this._projectservce.deleteProject(id).subscribe({
  //     next:(res)=>{
  //       this._coreservice.openSnackBar('Project Deleted!','done')
  //       this.getProjects();
  //     },
  //     error:console.log,
  //   })
  // }
  deleteProject(id: number): void {
    const dialogRef = this._dialog.open(ConfirmationDialogComponent, {
      width: '250px',
      data: 'Are you sure you want to delete this project?'
    });
  
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this._projectservce.deleteProject(id).subscribe({
          next: (res) => {
            this._coreservice.openSnackBar('Project Deleted!', 'done');
            this.getProjects();
          },
          error: console.log
        });
      }
    });
  }
  editForm(data: any) {
    const dialogRef=this._dialog.open(AddProjectComponent, {
        data,
      });
      dialogRef.afterClosed().subscribe({
        next:(val)=>{
          if(val){
            this.getProjects();
          }
        },
       });
   }
   
}
