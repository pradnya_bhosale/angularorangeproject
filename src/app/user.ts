export class User {
    
   
    id: number;
    fullName: string;
    email: string;
    username: string;
    password: string;
    isActive: boolean;
    createdAt: Date;
    updatedAt: Date;
    deletedAt: Date;
  
    constructor(
      id: number,
      fullName: string,
      email: string,
      username: string,
      password: string,
      isActive: boolean,
      createdAt: Date,
      updatedAt: Date,
      deletedAt: Date
    ) {
      this.id = id;
      this.fullName = fullName;
      this.email = email;
      this.username = username;
      this.password = password;
      this.isActive = isActive;
      this.createdAt = createdAt;
      this.updatedAt = updatedAt;
      this.deletedAt = deletedAt;
    }
  }
  