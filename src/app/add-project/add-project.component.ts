import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ProjectService } from '../service/project.service';
import {  MAT_DIALOG_DATA } from '@angular/material/dialog';
import { CoreService } from '../core/core.service';
import { MatDialogRef,MatDialogModule } from '@angular/material/dialog'; 
import { User } from '../user';
import { Project } from '../projects';

@Component({
  selector: 'app-add-project',
  templateUrl: './add-project.component.html',
  styleUrls: ['./add-project.component.scss']
})
export class AddProjectComponent implements OnInit {
  addForm: FormGroup;

  constructor(
    private _fb: FormBuilder,
    private _projectService: ProjectService,
   private dialogRef: MatDialogRef<AddProjectComponent> ,// Correct injection of MatDialogRef
    @Inject(MAT_DIALOG_DATA) public data: any,
    private _coreservice: CoreService
  ) {
    this.addForm = this._fb.group({
      title: ['', Validators.required],
      completedAt: ['', Validators.required],
      deletedAt: ['', Validators.required],
    });
  }

  ngOnInit(): void {
    if (this.data) {
      this.addForm.patchValue(this.data);
    }
  }

  onFormSubmit(): void {
    if (this.addForm.valid) {
      if (this.data) {
        this._projectService.updateProjects(this.data.id, this.addForm.value).subscribe({
          next: (val: any) => {
            this._coreservice.openSnackBar('Project Updated!', 'done');
            this.dialogRef.close(true); // Close dialog after update
          },
          error: (err: any) => {
            console.error(err);
          }
        });
      } else {
        this._projectService.addProjects(this.addForm.value).subscribe({
          next: (val: any) => {
            this._coreservice.openSnackBar('Project added successfully!', 'done');
            this.dialogRef.close(true); // Close dialog after add
          },
          error: (err: any) => {
            console.error(err);
          }
        });
      }
    }
  }
  closeDialog(): void {
    this.dialogRef.close();
  }
}
