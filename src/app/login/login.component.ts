import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';
import { User } from '../user'; // Assuming User interface or model is defined


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {
  formGroup: FormGroup;

  constructor(
    private authService: AuthService,
    private fb: FormBuilder,
    private router: Router,
   
   
  ) {
    this.formGroup = this.fb.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

  loginUser(): void {
    if (this.formGroup.valid) {
      const userData: User = this.formGroup.value;
      this.authService.login(userData).subscribe({
        next: (response: any) => {
          console.log('Login successful:', response);
          alert("login sucessfully")
          // Handle successful login (e.g., redirect to dashboard)
        this.router.navigate(['/projects']);

        },
        error: (error: any) => {
          console.error('Login error:', error);

          // Handle error (e.g., display error message)
          alert('Login failed. Please try again.'); // Example of handling error with alert
        }
      });
    }
  }
}
