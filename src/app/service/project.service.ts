import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Project } from '../projects';

@Injectable({
  providedIn: 'root'
})
export class ProjectService {

  constructor( private http:HttpClient) { }
  addProjects(data:Project):Observable<Project>{
    return this.http.post<Project>('http://localhost:8585/api/v1/projects',data)
  }
  updateProjects(id:number,data:Project):Observable<Project>{
    return this.http.put<Project>(`http://localhost:8585/api/v1/projects/${id}`,data)
  }
  getProjects():Observable<Project[]>{
    return this.http.get<Project[]>('http://localhost:8585/api/v1/projects')
  }
 
  deleteProject(id: number): Observable<Project> {
    return this.http.delete<Project>(`http://localhost:8585/api/v1/projects/${id}`);
}
}