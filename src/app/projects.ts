import { User } from './user';

export class Project {
  id: number;
  title: string;
  completedAt: Date;
  createdAt: Date;
  updatedAt: Date;
  deletedAt: Date;
  user?: User;

  constructor(
    id: number,
    title: string,
    completedAt: Date,
    createdAt: Date,
    updatedAt: Date,
    deletedAt: Date,
    user?: User // Optional user property
  ) {
    this.id = id;
    this.title = title;
    this.completedAt = completedAt;
    this.createdAt = createdAt;
    this.updatedAt = updatedAt;
    this.deletedAt = deletedAt;
    this.user = user;
  }
}
