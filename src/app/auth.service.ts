import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { User } from './user'; // Assuming User interface or model is defined correctly

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http: HttpClient) {}

  login(data: User): Observable<User> {
    return this.http.post<User>('http://localhost:8585/api/v1/users/login', data);
  }
}
